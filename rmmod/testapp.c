/*++

Copyright (c) 1990-98  Microsoft Corporation All Rights Reserved

Module Name:

    testapp.c

Abstract:

Environment:

    Win32 console multi-threaded application

--*/
#include <windows.h>
#include <winioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strsafe.h>
#include "codes.h"


BOOLEAN
ManageDriver(
    _In_ LPCTSTR  DriverName,
    _In_ LPCTSTR  ServiceName,
    _In_ USHORT   Function
    );

BOOLEAN
SetupDriverName(
    _Inout_updates_bytes_all_(BufferLength) PCHAR DriverLocation,
    _In_ ULONG BufferLength, _In_ PCHAR DriverName
    );

char OutputBuffer[100];
char InputBuffer[100];

VOID __cdecl
main(
	_In_ ULONG argc,
	_In_reads_(argc) PCHAR argv[]
)
{
	TCHAR driverLocation[MAX_PATH];

	//UNREFERENCED_PARAMETER(argc);
	//UNREFERENCED_PARAMETER(argv);

	if (argc < 2)
	{
		printf("Usage: %s FILENAME\n", argv[0]);
		printf("\tFILENAME: For example KerLogger.sys\n");
		exit(EXIT_FAILURE);
	}

	PCHAR DriverName = argv[1];
	printf("Got request to load %s\n", DriverName);

	//
	// The driver is not started yet so let us the install the driver.
	// First setup full path to driver name.
	//

	if (!SetupDriverName(driverLocation, sizeof(driverLocation), DriverName)) {
		return;
	}
	/*
	if (!ManageDriver(DriverName,
		driverLocation,
		DRIVER_FUNC_INSTALL
	)) {

		printf("Unable to install driver. \n");

		//
		// Error - remove driver.
		//

		ManageDriver(DriverName,
			driverLocation,
			DRIVER_FUNC_REMOVE
		);

		return;
	}*/

	//
	// Unload the driver.  Ignore any errors.
	//

	if (!ManageDriver(DriverName,
		driverLocation,
		DRIVER_FUNC_REMOVE
	))
	{
		printf("Unable to remove driver. \n");
		return;
	}
}


