#include <wdm.h>
#include <ntdef.h>

HANDLE hDevice = NULL;

static VOID write(PVOID msg) {
	IO_STATUS_BLOCK ioStatusBlock;

	NTSTATUS ret = ZwWriteFile(hDevice, NULL, NULL, NULL,
		&ioStatusBlock,
		(PVOID)msg,
		(ULONG)strlen(msg),
		NULL,
		NULL);

	if (STATUS_SUCCESS != ret) {
		DbgPrint("ERROR: ZwWriteFile: %d", ret);
	}
}

#define MAX_THREADS 5
PKTHREAD threads[MAX_THREADS];

NTSTATUS thread_init(PKTHREAD *thr, void *msg)
{
	HANDLE tmp = NULL;
	NTSTATUS status;

	status = PsCreateSystemThread(&tmp, THREAD_ALL_ACCESS, NULL, NULL, NULL, write, msg);
	if (!NT_SUCCESS(status)) {
		DbgPrint("ERROR: PsCreateSystemThread: %d", status);
		return status;
	}

	status = ObReferenceObjectByHandle(tmp, THREAD_ALL_ACCESS, NULL, KernelMode, (PVOID *)thr, NULL);
	if (status != STATUS_SUCCESS) 
		DbgPrint("ERROR: ObReferenceObjectByHandle: %d", status);
	ZwClose(tmp);
	return status;
}

char *messages[MAX_THREADS] = {
	"Kernel Message1\r\n",
	"Kernel Message2\r\n",
	"Kernel Message3\r\n",
	"Kernel Message4\r\n",
	"Kernel Message5\r\n"
};

NTSTATUS DriverEntry(struct _DRIVER_OBJECT *DriverObject, PUNICODE_STRING RegistryPath)
{
	DbgPrint("Driver Entry\n");
	UNREFERENCED_PARAMETER(RegistryPath);

	UNICODE_STRING     uniName;
	OBJECT_ATTRIBUTES  objAttr;
	IO_STATUS_BLOCK    ioStatusBlock;

	RtlInitUnicodeString(&uniName, L"\\Device\\KerLogger");
	InitializeObjectAttributes(&objAttr, &uniName, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

	NTSTATUS ntResult = ZwCreateFile(&hDevice,
		GENERIC_WRITE,
		&objAttr, &ioStatusBlock, NULL,
		FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_WRITE,
		FILE_OPEN_IF,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);

	if (ntResult != STATUS_SUCCESS) {
		DbgPrint("ERROR: ZwCreateFile: %d", ntResult);
		return ntResult;
	}

	for (ptrdiff_t i = 0; i<MAX_THREADS; i++) {
		if ((ntResult = thread_init(threads + i, messages[i])) != STATUS_SUCCESS)
			return ntResult;
	} 

	for (ptrdiff_t i = 0; i < MAX_THREADS; i++) {
		KeWaitForSingleObject(threads[i], 0, 0, FALSE, 0);
	}

	for (ptrdiff_t i = 0; i<MAX_THREADS; i++) {
		ObDereferenceObject(threads[i]);
	}

	ZwClose(hDevice);
	return STATUS_SUCCESS;
}