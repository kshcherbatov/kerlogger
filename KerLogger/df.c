#include <wdm.h>
#include "df.h"

void DF_Init(struct DPC_Flusher *df, PKDEFERRED_ROUTINE routine, PVOID ctx)
{
	KeInitializeDpc(&df->dpc, routine, ctx);
	df->is_queued = 0;
}

void DF_Enqueue(struct DPC_Flusher *df)
{
	LONG is_queued = InterlockedCompareExchange(&df->is_queued, 1, 0);
	if (is_queued) {
		KeInsertQueueDpc(&df->dpc, NULL, NULL);
	}
}

void DF_Deinit(struct DPC_Flusher *df)
{
	KeRemoveQueueDpc(&df->dpc);
}