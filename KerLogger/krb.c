#include "mem.h"
#include "msg.h"
#include "krb.h"

struct KernelRingBuffer {
	ULONG size;

	// Goes from 0 to MAX_ULONG, should be %size before using
	ULONG writeIdx;
	// Goes from 0 to size-1, should be %size when ++
	ULONG flushIdx;

	msg_t data[0];
};

typedef struct KernelRingBuffer krb_t;

krb_t *KRB_Construct(POOL_TYPE type, ULONG size)
{
	if (size == 0) {
		return NULL;
	}
	krb_t *krb = (krb_t *)K_allocate(type, sizeof(krb_t)+size*sizeof(msg_t));
	if (krb == NULL) {
		return NULL;
	}
	krb->size = size;
	krb->flushIdx = 0;
	krb->writeIdx = 0;
	for (ptrdiff_t i = 0; i < size; i++) {
		krb->data[i].state = MSG_STATE_EMPTY;
		krb->data[i].len = 0;
	}
	return krb;
}

void KRB_Destruct(struct KernelRingBuffer* krb)
{
	K_free(krb);
}

int KRB_WriteData(struct KernelRingBuffer *krb, const void *src, size_t src_size)
{
	const volatile LONG currWriteIdx = (InterlockedIncrement(&krb->writeIdx) - 1) % krb->size;
	return msg_fill(krb->data + currWriteIdx, src, src_size) > 0 ? 0 : -1;
}

void KRB_ProcessData(struct KernelRingBuffer *krb, mem_handler_t handler, void *param)
{
	const volatile int writeIdx = krb->writeIdx % krb->size;

	int ret = 0;
	while (krb->flushIdx != writeIdx && ret == 0) {
		ret = msg_export(krb->data + krb->flushIdx, handler, param);
		if (ret == 0) {
			krb->flushIdx++;
			krb->flushIdx %= krb->size;
		}
	}
}