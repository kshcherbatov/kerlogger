#pragma once

#include <wdm.h>
#include "msg.h"

struct KernelRingBuffer;

struct KernelRingBuffer* KRB_Construct(POOL_TYPE type, ULONG size);
void KRB_Destruct(struct KernelRingBuffer* krb);
int KRB_WriteData(struct KernelRingBuffer *krb, const void *src, size_t size);
void KRB_ProcessData(struct KernelRingBuffer *krb, mem_handler_t handler, void *param);