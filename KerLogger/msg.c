#include <wdm.h>
#include "msg.h"

size_t msg_fill(msg_t *msg, const void *src, size_t size)
{
	msg_state_t init_state = MSG_STATE_EMPTY;

	// Set state from Empty->Locked
	init_state = _InterlockedCompareExchange((long *)&msg->state, MSG_STATE_LOCKED, MSG_STATE_EMPTY);
	// We could not set from Empty->Locked, maybe Written?
	if (init_state == MSG_STATE_WRITTEN) {
		init_state = _InterlockedCompareExchange((long *)&msg->state, MSG_STATE_LOCKED, MSG_STATE_WRITTEN);
	}

	// Check if Locked (aka prev was Empty or Written)
	if (init_state != MSG_STATE_EMPTY && init_state != MSG_STATE_WRITTEN) {
		DbgBreakPoint();
		DbgPrint("DATA LOSS: rw conflict: %.*s", (int)size, src);
		return 0; // data loss, sorry -- cannot wait (deadlock)
	}
	if (init_state == MSG_STATE_WRITTEN) {
		DbgBreakPoint();
		DbgPrint("DATA LOSS: ww conflict: %.*s", (int)size, src);
	}

	msg->len = (size < MAX_MSG_LEN) ? size : MAX_MSG_LEN;
	RtlCopyMemory(msg->content, src, msg->len);
	_InterlockedExchange((long *)&msg->state, MSG_STATE_WRITTEN);
	return msg->len;
}

int msg_export(msg_t *msg, mem_handler_t handler, void *param)
{
	msg_state_t init_state = _InterlockedCompareExchange((long *)&msg->state, MSG_STATE_LOCKED, MSG_STATE_WRITTEN);
	if (init_state != MSG_STATE_WRITTEN) {
		return -1;
	}
	int ret = handler(param, msg->content, msg->len);
	if (ret != 0)
		DbgPrint("ERROR: BLOCKED cell: %.*s", (int)msg->len, msg->content);

	_InterlockedExchange((long *)&msg->state, MSG_STATE_EMPTY);
	return ret;
}