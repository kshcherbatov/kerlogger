#include <wdm.h>
#include "mem.h"

#define POOL_TAG 'SKNB'

void *K_allocate(POOL_TYPE type, size_t size)
{
	return ExAllocatePoolWithTag(
		_In_ type,
		_In_ size,
		_In_ POOL_TAG
	);
}

void K_free(void *data)
{
	if (data != NULL) {
		ExFreePoolWithTag(
			_In_ data,
			_In_ POOL_TAG
		);
	}
}