#pragma once

#include <wdm.h>

struct ThreadFlusher {
	PKTHREAD thread;
	KEVENT flush_event;
	LONG shutdown_flag;
};

typedef void(*tf_callback_t)(void *param);

int TF_Init(struct ThreadFlusher *tf, PKSTART_ROUTINE task, void *arg);
void TF_Deinit(struct ThreadFlusher *tf);
void TF_LoopedInvoke(struct ThreadFlusher *tf, tf_callback_t callback, void *param);