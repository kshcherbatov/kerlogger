#include <Ntifs.h>
#include <Ntdef.h>
#include <Wdm.h>

#include "mem.h"
#include "krb.h"
#include "kfw.h"
#include "tf.h"
#include "df.h"

#include "kl.h"

static VOID KL_paged2file_flushTask(PVOID ptr);
static VOID KL_resident2paged_flushTask(struct _KDPC *dpc, PVOID _kl, PVOID dummy, PVOID dummy2);

#define MAX_RESIDENT_MSG_NUM 16LLU
#define MAX_PAGED_MSG_NUM    256LLU

struct KernelLogger {
	struct KernelRingBuffer *resident_buf;
	struct KernelRingBuffer *paged_buf;

	struct DPC_Flusher resident2paged_flusher;

	struct ThreadFlusher    paged2file_flusher;
	struct KernelFileWriter file_writer;
};
typedef struct KernelLogger kl_t;

kl_t *KL_Construct(PCWSTR filename)
{
	PAGED_CODE();

	kl_t *kl = (kl_t *)K_allocate(NonPagedPool, sizeof(kl_t));
	if (!kl)
		return NULL;

	kl->resident_buf = KRB_Construct(NonPagedPool, MAX_RESIDENT_MSG_NUM);
	if (!kl->resident_buf)
		goto failure;

	kl->paged_buf = KRB_Construct(PagedPool, MAX_PAGED_MSG_NUM);
	if (!kl->paged_buf)
		goto failure;

	int kfw_init_failed = KFW_Init(&kl->file_writer, filename);
	if (kfw_init_failed)
		goto failure;

	int tf_init_failed = TF_Init(&kl->paged2file_flusher, KL_paged2file_flushTask, kl);
	if (tf_init_failed)
		goto failure;

	DF_Init(&kl->resident2paged_flusher, KL_resident2paged_flushTask, (PVOID)kl);

	return kl;

failure:
	KRB_Destruct(kl->resident_buf);
	KRB_Destruct(kl->paged_buf);
	if (!kfw_init_failed) {
		KFW_Deinit(&kl->file_writer);
	}
	return NULL;
}

void KL_Destruct(struct KernelLogger* kl) {
	if (kl == NULL)
		return;

	PAGED_CODE();
	DF_Deinit(&kl->resident2paged_flusher); // drop dpc task for non-paged buf
	KRB_ProcessData(kl->resident_buf, (mem_handler_t)KRB_WriteData, kl->paged_buf); // flush non-paged buf
	TF_Deinit(&kl->paged2file_flusher);     // flush paged buf to file & stop thread
	KFW_Deinit(&kl->file_writer);
	KRB_Destruct(kl->resident_buf);
	KRB_Destruct(kl->paged_buf);
	K_free(kl);
}

void KL_WriteMsg(struct KernelLogger *kl, const void *msg, size_t size)
{
	KIRQL irql = KeGetCurrentIrql();
	// TODO (?): retry for paged buffer if written_amount < strlen(msg)
	KRB_WriteData(irql > DISPATCH_LEVEL ? kl->resident_buf : kl->paged_buf, msg, size);
	if (irql > DISPATCH_LEVEL) {
		DF_Enqueue(&kl->resident2paged_flusher);
	}
}

static VOID KL_resident2paged_flushTask(struct _KDPC *dpc, PVOID _kl, PVOID dummy, PVOID dummy2)
{
	kl_t *kl = (kl_t *)_kl;
	KRB_ProcessData(kl->resident_buf, (mem_handler_t)KRB_WriteData, kl->paged_buf);
	kl->resident2paged_flusher.is_queued = 0;
}

static void KL_paged2file_flushTask_callback(void *_kl) {
	kl_t *kl = (kl_t *)_kl;
	// FIXME:
	// remember about irql!!!! -- cannot work with paged pool on high irql
	// ? - maybe we're invoked only on low irqls?
	KRB_ProcessData(kl->paged_buf, (mem_handler_t)KFW_Write, &kl->file_writer);
}

static VOID KL_paged2file_flushTask(PVOID ptr)
{
	kl_t *kl = (kl_t *)ptr;
	TF_LoopedInvoke(&kl->paged2file_flusher, KL_paged2file_flushTask_callback, ptr);
}