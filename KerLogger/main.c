#include <ntdef.h>
#include <ntstatus.h>
#include <wdm.h>
#include "kl.h"
#include "mem.h"
#include <Ntstrsafe.h>

static struct KernelLogger *logger = NULL;
DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD UnloadDriver;
DRIVER_DISPATCH CreateClose, Write;

#define NT_DEVICE_NAME      L"\\Device\\KerLogger"
#define DOS_DEVICE_NAME     L"\\DosDevices\\KerLoggerWin32"

#define TEST_THREAD_AMOUNT 2
static PKTIMER timer[TEST_THREAD_AMOUNT];
static PRKDPC dpc[TEST_THREAD_AMOUNT];
static unsigned msg_counter[TEST_THREAD_AMOUNT];

VOID DpcTestThread(PKDPC Dpc, PVOID _thr_num, PVOID dummy1, PVOID  dummy2)
{
	ULONG64 thr_num = (ULONG64)_thr_num;

	char pszDest[30];
	size_t cchDest = sizeof(pszDest);
	RtlZeroMemory(pszDest, cchDest);
	NTSTRSAFE_PSTR pszFormat = "source %u msg_id %llu irql %d\r\n";

	if (msg_counter[thr_num] % 2) {
		KIRQL cur_irql;
		NTSTATUS status =
			RtlStringCchPrintfA(pszDest, cchDest, pszFormat, thr_num, msg_counter[thr_num], HIGH_LEVEL);

		KeRaiseIrql(HIGH_LEVEL, &cur_irql);
		KL_WriteMsg(logger, pszDest, strlen(pszDest));
		// DbgPrint("KLogger: test write high");
		KeLowerIrql(cur_irql);
	} else {
		NTSTATUS status =
			RtlStringCchPrintfA(pszDest, cchDest, pszFormat, thr_num, msg_counter[thr_num], KeGetCurrentIrql());
	
		KL_WriteMsg(logger, pszDest, strlen(pszDest));
		// DbgPrint("KLogger: test write dpc");
	}

	msg_counter[thr_num]++;
}

void StopTestThreads(void)
{
	if (!timer[0])
		return;

	KeFlushQueuedDpcs();
	for (unsigned i = 0; i < TEST_THREAD_AMOUNT; i++) {
		KeCancelTimer(timer[i]);
		K_free(timer[i]);
		K_free(dpc[i]);
	}
}

int SpawnTestThreads(void)
{
	RtlZeroMemory(timer, sizeof(timer));
	RtlZeroMemory(dpc, sizeof(dpc));

	for (unsigned i = 0; i < TEST_THREAD_AMOUNT; i++) {
		if (!(timer[i] = K_allocate(NonPagedPool, sizeof(KTIMER))))
			goto failure;
		if (!(dpc[i] = K_allocate(NonPagedPool, sizeof(KDPC))))
			goto failure;
	}

	LARGE_INTEGER timeout = {.QuadPart = -1000000LL}; // 100ms, because time in 100ns format
	LONG period = 1000; // 1s, because time in 1ms format

	for (unsigned i = 0; i < TEST_THREAD_AMOUNT; i++) {
		KeInitializeTimer(timer[i]);
		KeInitializeDpc(dpc[i], DpcTestThread, (PVOID)i);
		KeSetTimerEx(timer[i], timeout, period, dpc[i]);
	}
	return 0;

failure:
	for (unsigned i = 0; i < TEST_THREAD_AMOUNT; i++) {
		K_free(timer[i]);
		K_free(dpc[i]);
	}
	return -1;
}

NTSTATUS DriverEntry(struct _DRIVER_OBJECT *DriverObject, PUNICODE_STRING RegistryPath)
{
	DbgPrint("Driver Entry\n");
	UNREFERENCED_PARAMETER(RegistryPath);

	DriverObject->MajorFunction[IRP_MJ_CREATE] = CreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = CreateClose;
	DriverObject->MajorFunction[IRP_MJ_WRITE] = Write;
	DriverObject->DriverUnload = UnloadDriver;

	UNICODE_STRING  ntUnicodeString;     // NT Device Name
	UNICODE_STRING  ntWin32NameString;   // Win32 Name
	PDEVICE_OBJECT  deviceObject = NULL;

	RtlInitUnicodeString(&ntUnicodeString, NT_DEVICE_NAME);
	NTSTATUS ntStatus = IoCreateDevice(
		DriverObject,                   // Our Driver Object
		0,                              // We don't use a device extension
		&ntUnicodeString,               // Device name "\Device\KerLogger"
		FILE_DEVICE_UNKNOWN,            // Device type
		FILE_DEVICE_SECURE_OPEN,        // Device characteristics
		FALSE,                          // Not an exclusive device
		&deviceObject);                 // Returned ptr to Device Object

	deviceObject->Flags |= DO_BUFFERED_IO;

	if (!NT_SUCCESS(ntStatus))
		return ntStatus;

	RtlInitUnicodeString(&ntWin32NameString, DOS_DEVICE_NAME);

	if (!NT_SUCCESS(IoCreateSymbolicLink(&ntWin32NameString, &ntUnicodeString)))
		IoDeleteDevice(deviceObject);

	if ((logger = KL_Construct(TEXT("\\SystemRoot\\kerlogger_test.txt"))) == NULL) {
		return STATUS_DRIVER_INTERNAL_ERROR;
	}

	// return STATUS_SUCCESS;
	return SpawnTestThreads();
}

VOID UnloadDriver(struct _DRIVER_OBJECT *DriverObject)
{
	DbgPrint("Unload\n");
	PDEVICE_OBJECT deviceObject = DriverObject->DeviceObject;
	UNICODE_STRING uniWin32NameString;

	PAGED_CODE();
	
	StopTestThreads();

	RtlInitUnicodeString(&uniWin32NameString, DOS_DEVICE_NAME);
	IoDeleteSymbolicLink(&uniWin32NameString);

	if (deviceObject != NULL)
	{
		IoDeleteDevice(deviceObject);
	}
	KL_Destruct(logger);
}

// does nothing, return success
NTSTATUS CreateClose(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	PAGED_CODE();

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;

	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

NTSTATUS Write(PDEVICE_OBJECT DeviceObject, PIRP pIrp)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	PIO_STACK_LOCATION pIrpStack = IoGetCurrentIrpStackLocation(pIrp);

	ULONG xferSize = pIrpStack->Parameters.Write.Length;
	PVOID userBuffer = pIrp->AssociatedIrp.SystemBuffer;

	KL_WriteMsg(logger, userBuffer, xferSize);

	pIrp->IoStatus.Status = STATUS_SUCCESS;
	pIrp->IoStatus.Information = xferSize;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}
