#pragma once

#include <Ntdef.h>

typedef int(*mem_handler_t)(void *param, const char *raw, size_t size);

#define MAX_MSG_LEN 256LLU

enum {
	MSG_STATE_EMPTY   = 0,
	MSG_STATE_LOCKED  = 1,
	MSG_STATE_WRITTEN = 2,
};
typedef UINT32 msg_state_t;

typedef struct {
	char content[MAX_MSG_LEN];
	msg_state_t state;
	size_t len;
} msg_t;

size_t msg_fill(msg_t *msg, const void *src, size_t size);
int msg_export(msg_t *msg, mem_handler_t handler, void *param);