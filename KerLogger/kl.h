#pragma once

#include <Ntdef.h>

struct KernelLogger;

struct KernelLogger *KL_Construct(PCWSTR filename);
void KL_Destruct(struct KernelLogger* kl);
void KL_WriteMsg(struct KernelLogger* kl, const void *msg, size_t size);
