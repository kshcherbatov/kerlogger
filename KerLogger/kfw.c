#include <ntdef.h>
#include <wdm.h>
#include "kfw.h"

int KFW_Init(struct KernelFileWriter *kfw, PCWSTR filename)
{
	UNICODE_STRING     uniName;
	OBJECT_ATTRIBUTES  objAttr;
	NTSTATUS ntResult;

	IO_STATUS_BLOCK    ioStatusBlock;	// the caller can determine the cause of the failure by checking this value

	RtlInitUnicodeString(&uniName, filename);  // or L"\\SystemRoot\\example.txt"
	InitializeObjectAttributes(&objAttr, &uniName,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
		NULL, NULL);

	// TODO: get reason from ioStat... DbgPrint
	ntResult = ZwCreateFile(&kfw->hFile,
		GENERIC_WRITE,
		&objAttr, &ioStatusBlock, NULL,
		FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		FILE_OPEN_IF,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);

	return ntResult == STATUS_SUCCESS ? 0 : -1;
}

void KFW_Deinit(struct KernelFileWriter* kfw)
{
	ZwClose(kfw->hFile);
}

int KFW_Write(struct KernelFileWriter* kfw, void *data, size_t size)
{
	NTSTATUS ret;
	IO_STATUS_BLOCK ioStatusBlock;

	ret = ZwWriteFile(kfw->hFile, NULL, NULL, NULL,
		&ioStatusBlock,
		(PVOID)data,
		(ULONG)size,
		NULL,
		NULL);

	if (STATUS_SUCCESS != ret)
		return -1;

	return 0;
}