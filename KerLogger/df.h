#pragma once

#include <wdm.h>

struct DPC_Flusher {
	KDPC dpc;
	LONG is_queued;
};

void DF_Init(struct DPC_Flusher *df, PKDEFERRED_ROUTINE routine, PVOID ctx);
void DF_Deinit(struct DPC_Flusher *df);
void DF_Enqueue(struct DPC_Flusher *df);