#include <wdm.h>
#include "tf.h"

int TF_Init(struct ThreadFlusher *tf, PKSTART_ROUTINE task, void *arg)
{
	tf->shutdown_flag = 0;
	KeInitializeEvent(&tf->flush_event, NotificationEvent, FALSE);

	HANDLE tmp = NULL;
	NTSTATUS status;

	status = PsCreateSystemThread(
		&tmp,
		THREAD_ALL_ACCESS,
		NULL,
		NULL,
		NULL,
		task,
		arg);

	if (!NT_SUCCESS(status)) {
		DbgPrint("KLogger: flush thread - error creating\n");
		return -1;
	}

	status = ObReferenceObjectByHandle(
		tmp,
		THREAD_ALL_ACCESS,
		NULL,
		KernelMode,
		(PVOID *)&tf->thread,
		NULL);

	ZwClose(tmp);
	return status == STATUS_SUCCESS ? 0 : -1;
}

void TF_Deinit(struct ThreadFlusher *tf)
{
	tf->shutdown_flag = 1;
	KeSetEvent(&tf->flush_event, 0, FALSE);

	KeWaitForSingleObject(
		tf->thread,
		Executive,
		KernelMode,
		FALSE,
		NULL);

	ObDereferenceObject(tf->thread);
	ZwClose(&tf->flush_event);
}

void TF_LoopedInvoke(struct ThreadFlusher *tf, tf_callback_t callback, void *param)
{
	PVOID handles[1] = { (PVOID)&tf->flush_event };
	LARGE_INTEGER timeout;
	timeout.QuadPart = -100000000LL; // 10 sec, because time in 100ns format

	while (!tf->shutdown_flag) {
		NTSTATUS status = KeWaitForMultipleObjects(
			1,
			handles,
			WaitAny,
			Executive,
			KernelMode,
			TRUE,
			&timeout,
			NULL);

		callback(param);
		KeClearEvent(&tf->flush_event);
	}
	PsTerminateSystemThread(STATUS_SUCCESS);
}