#pragma once

#include <ntdef.h>

struct KernelFileWriter {
	HANDLE hFile;
};

int KFW_Init(struct KernelFileWriter *kfw, PCWSTR filename);
void KFW_Deinit(struct KernelFileWriter* kfw);
int KFW_Write(struct KernelFileWriter* kfw, void *data, size_t size);