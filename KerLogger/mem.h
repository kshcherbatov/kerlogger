#pragma once

#include <wdm.h>

void *K_allocate(POOL_TYPE type, size_t size);
void K_free(void *data);