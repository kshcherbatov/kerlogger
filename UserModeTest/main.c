#include <stdio.h>
#include <Windows.h>

HANDLE hDevice = INVALID_HANDLE_VALUE;

static DWORD WINAPI write(LPVOID msg) {
	DWORD written;
	if (!WriteFile(hDevice, msg, (DWORD)strlen(msg), &written, NULL)) {
		fprintf(stderr, "ERROR: WriteFile: %d\n", GetLastError());
		return 1;
	}
	return 0;
}

#define MAX_THREADS 5
HANDLE threads[MAX_THREADS];
DWORD threadIds[MAX_THREADS];

char *messages[MAX_THREADS] = {
	"User Message1\r\n",
	"User Message2\r\n",
	"User Message3\r\n",
	"User Message4\r\n",
	"User Message5\r\n"
};

int main() {
	hDevice = CreateFile("\\\\.\\KerLoggerWin32",
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hDevice == INVALID_HANDLE_VALUE) {
		fprintf(stderr, "ERROR: CreateFile: %d\n", GetLastError());
		return 1;
	}

	for (ptrdiff_t i = 0; i<MAX_THREADS; i++) {
		threads[i] = CreateThread(NULL, 0, write, messages[i], 0, threadIds+i);
		if (threads[i] == NULL) {
			fprintf(stderr, "ERROR: CreateThread: %d\n", GetLastError());
			return 1;
		}
	} 

	WaitForMultipleObjects(MAX_THREADS, threads, TRUE, INFINITE);

	for (ptrdiff_t i = 0; i<MAX_THREADS; i++) {
		CloseHandle(threads[i]);
	}

	if (!CloseHandle(hDevice)) {
		fprintf(stderr, "ERROR: CloseHandle: %d\n", GetLastError());
		return 1;
	}
	return 0;
}